package com.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.TextDao;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.dto.TextDto;
import com.entity.Text;
import com.service.TextService;
@Service("textService")
public class TextServiceImpl implements TextService{
    @Resource
    private TextDao textDao;
    @Override
    public Pagers textMsgList(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	Pagers pagers= textDao.getForPage(pageinfo);
	List<Text> list=(List<Text>) pagers.getRows();
	List<TextDto> dtos=new ArrayList<TextDto>();
	for (Text text : list) {
	    TextDto dto=new TextDto();
	    BeanUtils.copyProperties(text, dto);
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }
    @Override
    public void SaveTextMsg(List<TextDto> dtos) {
	// TODO Auto-generated method stub
	Timestamp timestamp=new Timestamp(System.currentTimeMillis());
	System.out.println("=============");
	System.out.println(timestamp);
	System.out.println(dtos.size());
	System.out.println(dtos.get(0));
	TextDto dto=(TextDto)dtos.get(0);
	System.out.println(dto);
	
	for (TextDto textDto : dtos) {
	    Text text=new Text();
	    BeanUtils.copyProperties(textDto, text,"id");
	    text.setTime(timestamp);
	    text.setEdittime(timestamp);
	    textDao.save(text);
	}
    }
    @Override
    public void UpdateTextMsg(List<TextDto> dtos) {
	// TODO Auto-generated method stub
	Timestamp timestamp=new Timestamp(System.currentTimeMillis());
	for (TextDto textDto : dtos) {
	    if(textDto.getId()<0){
		Text text=new Text();
		
		    BeanUtils.copyProperties(textDto, text,"id");
		    text.setTime(timestamp);
		    text.setEdittime(timestamp);
		    textDao.save(text);
	    }
	    Text text=textDao.findById(textDto.getId());
	    if(text!=null){
		BeanUtils.copyProperties(textDto, text,"id");
		text.setEdittime(timestamp);
	    }
	}
    }
    @Override
    public void DelTextMsg(Integer[] ids) {
	// TODO Auto-generated method stub
	textDao.DelByIntids(ids);
    }

}
