package com.entity;

/**
 * Music entity. @author MyEclipse Persistence Tools
 */

public class Music implements java.io.Serializable {

    // Fields

    private Integer id;
    private User user;
    private String title;
    private String descri;
    private String musicurl;
    private String hqmusicurl;

    // Constructors

    /** default constructor */
    public Music() {
    }

    /** minimal constructor */
    public Music(String title, String musicurl) {
	this.title = title;
	this.musicurl = musicurl;
    }

    /** full constructor */
    public Music(User user, String title, String descri, String musicurl,
	    String hqmusicurl) {
	this.user = user;
	this.title = title;
	this.descri = descri;
	this.musicurl = musicurl;
	this.hqmusicurl = hqmusicurl;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescri() {
	return this.descri;
    }

    public void setDescri(String descri) {
	this.descri = descri;
    }

    public String getMusicurl() {
	return this.musicurl;
    }

    public void setMusicurl(String musicurl) {
	this.musicurl = musicurl;
    }

    public String getHqmusicurl() {
	return this.hqmusicurl;
    }

    public void setHqmusicurl(String hqmusicurl) {
	this.hqmusicurl = hqmusicurl;
    }

}