package com.entity;

import java.sql.Timestamp;

/**
 * Voices entity. @author MyEclipse Persistence Tools
 */

public class Voices implements java.io.Serializable {

    // Fields

    private Integer id;
    private Mediaid mediaid;
    private User user;
    private Timestamp ctime;

    // Constructors

    /** default constructor */
    public Voices() {
    }

    /** full constructor */
    public Voices(Mediaid mediaid, User user, Timestamp ctime) {
	this.mediaid = mediaid;
	this.user = user;
	this.ctime = ctime;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Mediaid getMediaid() {
	return this.mediaid;
    }

    public void setMediaid(Mediaid mediaid) {
	this.mediaid = mediaid;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public Timestamp getCtime() {
	return this.ctime;
    }

    public void setCtime(Timestamp ctime) {
	this.ctime = ctime;
    }

}