package com.dto;

import java.util.List;

public class ArticleTypeDto {
    private Integer id;
    private String text;
    private String des;
    private List<ArticleTypeDto> children;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public List<ArticleTypeDto> getChildren() {
        return children;
    }
    public void setChildren(List<ArticleTypeDto> children) {
        this.children = children;
    }
    public String getDes() {
        return des;
    }
    public void setDes(String des) {
        this.des = des;
    }
    
}
