package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.ArticleTypeDao;
import com.entity.Articletype;
@Repository("articleTypeDao")
public class ArticleTypeDaoImpl extends BaseDaoImpl<Articletype> implements ArticleTypeDao{

}
