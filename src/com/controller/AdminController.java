package com.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.dto.MenuDto;
import com.dto.MsgRoleDto;
import com.dto.MsgprsDto;
import com.dto.NewsDto;
import com.dto.NewsItemDto;
import com.dto.Pageinfo;
import com.dto.TextDto;
import com.dto.TextRoleDto;
import com.dto.UserDto;
import com.dto.UserGroupDto;
import com.entity.User;
import com.service.ArticleService;
import com.service.ArticleTypeService;
import com.service.ConfigService;
import com.service.MenusService;
import com.service.MsgRoleService;
import com.service.MsgService;
import com.service.MsgprsService;
import com.service.NewsItemService;
import com.service.NewsService;
import com.service.NewsTypeService;
import com.service.TextRoleService;
import com.service.TextService;
import com.service.UserGroupService;
import com.service.UserService;
import com.vo.ArticleTypeVo;
import com.vo.ArticleVo;
import com.vo.NewsTypeVo;
/**
 * 
 * AdminController.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午12:56:06 
 * @version
 * @user micrxdd
 */
@Controller
@RequestMapping("/manager")
@SessionAttributes("managerUser")
public class AdminController {
    @Resource
    private MsgService msgService;
    @Resource
    private MenusService menusService;
    @Resource
    private TextService textService;
    @Resource
    private UserGroupService userGroupService;
    @Resource
    private UserService userService;
    @Resource
    private NewsService newsService;
    @Resource
    private NewsItemService newsItemService;
    @Resource
    private NewsTypeService newsTypeService;
    @Resource
    private MsgprsService msgprsService;
    @Resource
    private MsgRoleService msgRoleService;
    @Resource
    private TextRoleService textRoleService;
    @Resource
    private ArticleTypeService articleTypeService;
    @Resource
    private ArticleService articleService;
    @Resource
    private ConfigService configService;
    private Map<String, Object> maps;
    /**
     * 默认构造方法
     */
    public AdminController() {
	// TODO Auto-generated constructor stub
	maps=new HashMap<String, Object>();
	maps.put("success", true);
	maps.put("msg", "success");
    }
    /**
     * 每次更改了消息回复规则都要调用这个方法来重新初始化
     * 初始化会查询所有的规则列表，进行大量查询。
     * @return
     */
    @RequestMapping("initmsg")
    @ResponseBody
    public Object initMsgrole(){
	msgService.MsgRolesInit();
	return maps;
    }
    @RequestMapping("loginout")
    public String loginout(){
	Subject subject = SecurityUtils.getSubject();
	subject.logout();
	return "redirect:/login.html";
    }
    /**
     * 根据用户来获取菜单
     * @param user
     * @return
     */
    @RequestMapping("menu/menus")
    @ResponseBody
    public Object menus(){
	System.out.println("=================开始获取菜单");
	Object object=menusService.Menus(FindUser());
	System.out.println("===============开始获取菜单");
	return object;
    }
    /**
     * 获取所有的菜单
     * @return
     */
    @RequestMapping("menu/allmenus")
    @ResponseBody
    public Object ListAllMenus(){
	return menusService.AllMenus();
    }
    /**
     * 返回最基本的所有菜单
     * @return
     */
    @RequestMapping(value="menu/allmenus",params="type=base")
    @ResponseBody
    public Object ListAllMenuBasic(){
	return menusService.AllMenusBase();
    }
    @RequestMapping(value="menu/allmenus",params="type=group")
    @ResponseBody
    public Object MenusListByGroupId(Integer groupid){
	if(groupid==null)return null;
	return menusService.ListMenusByGroupId(groupid);
    }
    /**
     * 添加菜单接口
     * @param menuDtos
     * @return
     */
    @RequestMapping(value="menu/api",params="method=add")
    @ResponseBody
    public Object AddMenu(@RequestBody List<MenuDto> menuDtos){
	menusService.SaveMenus(menuDtos);
	return maps;
	
    }
    /**
     * 更新菜单接口
     * @param dtos
     * @return
     */
    @RequestMapping(value="menu/api",params="method=update")
    @ResponseBody
    public Object UpdateMenu(@RequestBody List<MenuDto> dtos){
	menusService.UpdateMenus(dtos);
	return maps;
    }
    /**
     * 删除菜单接口
     * @param ids
     * @return
     */
    @RequestMapping(value="menu/api",params="method=del")
    @ResponseBody
    public Object UpdateMenu(Integer[] ids){
	menusService.DeleteMenus(ids);
	return maps;
    }
    /**
     * 返回文本消息列表
     * @param pageinfo
     * @return
     */
    @RequestMapping("textmsg/list")
    @ResponseBody
    public Object Textmsgs(Pageinfo pageinfo){
	return textService.textMsgList(pageinfo);
    }
    /**
     * 保存文本消息
     * @param dtos
     * @return
     */
    @RequestMapping(value="textmsg/api",params="method=add")
    @ResponseBody
    public Object SaveTextMsg(@RequestBody List<TextDto> dtos){
	textService.SaveTextMsg(dtos);
	return maps;
    }
    /**
     * 更新文本消息 兼 保存文本消息
     * @param dtos
     * @return
     */
    @RequestMapping(value="textmsg/api",params="method=update")
    @ResponseBody
    public Object UpdateTextMsg(@RequestBody List<TextDto> dtos){
	textService.UpdateTextMsg(dtos);
	return maps;
    }
    /**
     * 删除文本消息
     * @param ids
     * @return
     */
    @RequestMapping(value="textmsg/api",params="method=del")
    @ResponseBody
    public Object DelTextMsg(Integer[] ids){
	textService.DelTextMsg(ids);
	return maps;
    }
    /**
     * 返回用户组列表
     * @param pageinfo
     * @return
     */
    @RequestMapping("usergroup/list")
    @ResponseBody
    public Object UserGroups(Pageinfo pageinfo){
	return userGroupService.Groups(pageinfo);
    }
    /**
     * 保存用户组列表
     * @param dtos
     * @return
     */
    @RequestMapping(value="usergroup/api",params="method=add")
    @ResponseBody
    public Object SaveUserGroups(@RequestBody List<UserGroupDto> dtos){
	userGroupService.SaveGroups(dtos);
	return maps;
    }
    /**
     * 更新用户组列表 兼 保存用户组列表
     * @param dtos
     * @return
     */
    @RequestMapping(value="usergroup/api",params="method=update")
    @ResponseBody
    public Object UpdateUserGroups(@RequestBody List<UserGroupDto> dtos){
	userGroupService.UpdateGroups(dtos);
	return maps;
    }
    @RequestMapping(value="usergroup/api",params="method=uprole")
    @ResponseBody
    public Object UpdateRole(Integer groupid,String ids){
	if(groupid==null)return null;
	userGroupService.UpdateRole(groupid, ids);
	return maps;
    }
    /**
     * 删除用户组列表
     * @param ids
     * @return
     */
    @RequestMapping(value="usergroup/api",params="method=del")
    @ResponseBody
    public Object DelUserGroups(Integer[] ids){
	userGroupService.DelGroups(ids);
	return maps;
    }
    /**
     * 返回用户列表
     * @param pageinfo
     * @return
     */
    @RequestMapping("user/list")
    @ResponseBody
    public Object UserList(Pageinfo pageinfo){
	return userService.UserList(pageinfo);
    }
    /**
     * 添加用户
     * @param dtos
     * @return
     */
    @RequestMapping(value="user/api",params="method=add")
    @ResponseBody
    public Object SaveUsers(@RequestBody List<UserDto> dtos){
	userService.SaveUsers(dtos);
	return maps;
    }
    /**
     * 更新用户 兼 保存用户
     * @param dtos
     * @return
     */
    @RequestMapping(value="user/api",params="method=update")
    @ResponseBody
    public Object UpdateUsers(@RequestBody List<UserDto> dtos){
	userService.UpdateUsers(dtos);
	return maps;
    }
    /**
     * 删除用户
     * @param ids
     * @return
     */
    @RequestMapping(value="user/api",params="method=del")
    @ResponseBody
    public Object DelUsers(Integer[] ids){
	userService.DelUserIds(ids);
	return maps;
    }
    /**
     * 返回新闻分类列表
     * @return
     */
    @RequestMapping("newstype/list")
    @ResponseBody
    public Object NewsTypeList(){
	return newsTypeService.NewsTypeList();
    }
    /**
     * 根据id返回这个新闻分类
     * @param id
     * @return
     */
    @RequestMapping(value="newstype/api",params="method=byid")
    @ResponseBody
    public Object NewsTypeById(Integer id){
	return newsTypeService.NewsTypeById(id);
    }
    /**
     * 添加一个新闻分类
     * @param newsTypeVo
     * @return
     */
    @RequestMapping(value="newstype/api",params="method=add")
    @ResponseBody
    public Object SaveNewsType(NewsTypeVo newsTypeVo){
	newsTypeService.SaveNewsType(newsTypeVo);
	return maps;
    }
    /**
     * 更新一个新闻分类
     * @param newsTypeVo
     * @return
     */
    @RequestMapping(value="newstype/api",params="method=update")
    @ResponseBody
    public Object UpdateNewType(NewsTypeVo newsTypeVo){
	newsTypeService.UpdateNewsType(newsTypeVo);
	return maps;
    }
    /**
     * 通过id删除一个新闻分类
     * @param id
     * @return
     */
    @RequestMapping(value="newstype/api",params="method=del")
    @ResponseBody
    public Object DelNewsType(Integer id){
	newsTypeService.DelNewsType(id);
	return maps;
    }
    /**
     * 返回新闻消息
     * @param pageinfo
     * @param typeid 新闻id
     * @return
     */
    @RequestMapping("newsitem/list")
    @ResponseBody
    public Object NewsItemList(Pageinfo pageinfo,Integer typeid){
	if(typeid==null) return null;
	return newsItemService.NewsLitemList(pageinfo, typeid);
    }
    /**
     * 保存新闻消息
     * @param dtos
     * @return
     */
    @RequestMapping(value="newsitem/api",params="method=add")
    @ResponseBody
    public Object SaveNewsItems(@RequestBody List<NewsItemDto> dtos){
	newsItemService.SaveNewsitem(dtos,FindUser());
	return maps;
    }
    @RequestMapping(value="newsitem/api",params="method=addnews")
    @ResponseBody
    public Object AddNewsIteNews(Integer[] ids,Integer itemsid){
	//System.out.println("你好------------------------");
	//System.out.println(itemsid);
	if(itemsid==null||ids==null)return null;
	newsService.UpdateNews(ids, itemsid);
	return maps;
    }
    /**
     * 更新新闻消息
     * @param dtos
     * @return
     */
    @RequestMapping(value="newsitem/api",params="method=update")
    @ResponseBody
    public Object UpdateNewsItems(@RequestBody List<NewsItemDto> dtos,@ModelAttribute("managerUser") User user){
	newsItemService.UpdateNewsitem(dtos,user);
	return maps;
    }
    /**
     * 删除新闻消息
     * @param ids
     * @return
     */
    @RequestMapping(value="newsitem/api",params="method=del")
    @ResponseBody
    public Object DelNewsItem(Integer[] ids){
	newsItemService.DelNewsitemIds(ids);
	return maps;
    }
    /**
     * 新闻消息
     * @param pageinfo
     * @param itemid
     * @return
     */
    @RequestMapping("news/list")
    @ResponseBody
    public Object NewsMsgList(Pageinfo pageinfo,Integer itemid){
	if(itemid==null)return null;
	return newsService.NewsList(pageinfo, itemid);
    }
    @RequestMapping(value="news/list",params="bind=noitem")
    @ResponseBody
    public Object NewsMsgListByNoItem(Pageinfo pageinfo){
	//System.out.println("sssssss");
	return newsService.NewsListNoItem(pageinfo);
    }
    /**
     * 添加新闻消息内容   新闻消息会自动生成  此处只是分配消息
     * @param dtos
     * @return
     */
    @RequestMapping(value="news/api",params="method=add2")
    @ResponseBody
    public Object SaveNewsMsg(NewsDto dto,Integer itemid){
	if(itemid==null)return null;
	newsService.SaveNews(dto,itemid);
	return maps;
    }
    @RequestMapping(value="news/api",params="method=add")
    @ResponseBody
    public Object SaveNewsMsg2(@RequestBody List<NewsDto> dto){
	newsService.SaveNewsDtos(dto);
	return maps;
    }
    /**
     * 更新新闻消息内容
     * @param dtos
     * @return
     */
    @RequestMapping(value="news/api",params="method=update")
    @ResponseBody
    public Object UpdateNewsMsg(@RequestBody List<NewsDto> dto){
	newsService.UpdateNewsDtos(dto);
	return maps;
    }
    /**
     * 删除新闻消息内容  此处指删除关系  不会删除对象
     * @param ids
     * @return
     */
    @RequestMapping(value="news/api",params="method=del")
    @ResponseBody
    public Object DelNewsMsg(Integer[] ids){
	newsService.DelNewsIds(ids);
	return maps;
    }
    /**
     * 返回消息分发器列表
     * @param pageinfo
     * @return
     */
    @RequestMapping("msgprs/list")
    @ResponseBody
    public Object MsgprsList(Pageinfo pageinfo){
	return msgprsService.MsgprsList(pageinfo);
    }
    /**
     * 添加一个消息分发器
     * @param dtos
     * @return
     */
    @RequestMapping(value="msgprs/api",params="method=add")
    @ResponseBody
    public Object SaveMsgprs(@RequestBody List<MsgprsDto> dtos){
	msgprsService.SaveMsgprs(dtos);
	return maps;
    }
    /**
     * 更新一个消息分发器
     * @param dtos
     * @return
     */
    @RequestMapping(value="msgprs/api",params="method=update")
    @ResponseBody
    public Object UpdateMsgprs(@RequestBody List<MsgprsDto> dtos){
	msgprsService.UpdateMsgprs(dtos);
	return maps;
    }
    /**
     * 删除一个消息分发器
     * @param ids
     * @return
     */
    @RequestMapping(value="msgprs/api",params="method=del")
    @ResponseBody
    public Object DelMsgPrs(Integer[] ids){
	msgprsService.DelMsgIntprs(ids);
	return maps;
    }
    /**
     * 返回消息规则列表
     * @param pageinfo
     * @return
     */
    @RequestMapping("msgrole/list")
    @ResponseBody
    public Object MsgRoles(Pageinfo pageinfo){
	return msgRoleService.MsgRoleList(pageinfo);
    }
    /**
     * 添加消息规则列表
     * @param dtos
     * @return
     */
    @RequestMapping(value="msgrole/api",params="method=add")
    @ResponseBody
    public Object SaveMsgRole(@RequestBody List<MsgRoleDto> dtos){
	msgRoleService.SaveMsgRoles(dtos);
	return maps;
    }
    /**
     * 更新消息规则列表
     * @param dtos
     * @return
     */
    @RequestMapping(value="msgrole/api",params="method=update")
    @ResponseBody
    public Object UpdateMsgRoles(@RequestBody List<MsgRoleDto> dtos){
	msgRoleService.UpdateMsgRoles(dtos);
	return maps;
    }
    /**
     * 删除消息规则列表
     * @param ids
     * @return
     */
    @RequestMapping(value="msgrole/api",params="method=del")
    @ResponseBody
    public Object DelMsgRoles(Integer[] ids){
	msgRoleService.DelMsgIntRoles(ids);
	return maps;
    }
    /**
     * 返回关键字规则表
     * @param pageinfo
     * @return
     */
    @RequestMapping("textrole/list")
    @ResponseBody
    public Object TextRoleList(Pageinfo pageinfo){
	return textRoleService.TextRoleList(pageinfo);
    }
    /**
     * 添加关键字规则
     * @param dtos
     * @return
     */
    @RequestMapping(value="textrole/api",params="method=add")
    @ResponseBody
    public Object SaveTextRoles(@RequestBody List<TextRoleDto> dtos){
	textRoleService.SaveTextRoles(dtos);
	return maps;
    }
    /**
     * 更新关键字规则
     * @param dtos
     * @return
     */
    @RequestMapping(value="textrole/api",params="method=update")
    @ResponseBody
    public Object UpdateTextRoles(@RequestBody List<TextRoleDto> dtos){
	textRoleService.UpdateTextRoles(dtos);
	return maps;
    }
    /**
     * 删除关键字规则
     * @param ids
     * @return
     */
    @RequestMapping(value="textrole/api",params="method=del")
    @ResponseBody
    public Object DelTextRoles(Integer[] ids){
	textRoleService.DelTextRoles(ids);
	return maps;
    }
    /**
     * 返回文章分类
     * @return
     */
    @RequestMapping("atype/list")
    @ResponseBody
    public Object ArticleTypeList(){
	return articleTypeService.ArticleTypeList();
    }
    /**
     * 添加一个文章分类
     * @param articleTypeVo
     * @return
     */
    @RequestMapping(value="atype/api",params="method=add")
    @ResponseBody
    public Object SaveArticleType(ArticleTypeVo articleTypeVo){
	articleTypeService.SaveArticleType(articleTypeVo);
	return maps;
    }
    /**
     * 更新一个文章分类
     * @param articleTypeVo
     * @return
     */
    @RequestMapping(value="atype/api",params="method=update")
    @ResponseBody
    public Object UpdateArticleType(ArticleTypeVo articleTypeVo){
	articleTypeService.UpdateArticleType(articleTypeVo);
	return maps;
    }
    /**
     * 删除一个文章分类
     * @param id
     * @return
     */
    @RequestMapping(value="atype/api",params="method=del")
    @ResponseBody
    public Object DelArticleType(Integer id){
	articleTypeService.DelArticleType(id);
	return maps;
    }
    /**
     * 根据文章分类id返回这个分类
     * @param id
     * @return
     */
    @RequestMapping(value="atype/api",params="method=byid")
    @ResponseBody
    public Object ArticleTypeById(Integer id){
	return articleTypeService.ArticleTyppById(id);
    }
    /**
     * 返回文章列表根据文章分类id
     * @param pageinfo
     * @param typeid
     * @return
     */
    @RequestMapping("article/list")
    @ResponseBody
    public Object ArticleList(Pageinfo pageinfo,Integer typeid){
	if(typeid==null)return null;
	return articleService.ArticleListById(pageinfo, typeid);
    }
    /**
     * 添加一个文章 文章id存在的话保存文章
     * @param user
     * @param articleVo
     * @return
     */
    @RequestMapping(value="article/api",params="method=add")
    @ResponseBody
    public Object SaveArticle(ArticleVo articleVo){
	//User user=(User) session.getAttribute("managerUser");
	User user=FindUser();
	System.out.println(user+"========================"+user.getUsername()+user.getId());
	articleService.SaveArticle(articleVo, user);
	return maps;
    }
    /**
     * 根据文章id获取一个文章
     * @param id
     * @return
     */
    @RequestMapping(value="article/api",params="method=byid")
    @ResponseBody
    public Object EditArticle(Integer id){
	if(id==null)return null;
	return articleService.FindVoById(id);
    }
    /**
     * 删除文章
     * @param ids
     * @return
     */
    @RequestMapping(value="article/api",params="method=del")
    @ResponseBody
    public Object DelArticle(Integer[] ids){
	articleService.DelArticle(ids);
	return maps;
    }
    @RequestMapping("initconfig")
    @ResponseBody
    public Object InitConfig(){
	configService.initConfig();
	return maps;
    }
    
    //@ModelAttribute("managerUser") User user
    public Map<String, Object> map(){
	return new HashMap<String, Object>();
    }
    public User FindUser(){
	Subject currentUser = SecurityUtils.getSubject();
	Session session = currentUser.getSession();
	return (User) session.getAttribute("managerUser");
    }
    
}
