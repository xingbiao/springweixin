 
package com.weixin.util;

public class WeixinMsg {
    /**
     * 事件类型小写的
     */
    public final static String EVENTCase="event";
    /**
     * 事件
     */
    public final static String EVENT="Event";
    /**
     * 开发者微信号
     */
    public final static String ToUserName="ToUserName";
    /**
     * 发送方帐号（一个OpenID）
     */
    public final static String FromUserName="FromUserName";
    /**
     * 消息创建时间 （整型）
     */
    public final static String CreateTime="CreateTime";
    /**
     * 消息类型，event
     */
    public final static String MsgType="MsgType";
    /**
     * 事件key
     */
    public final static String EventKey="EventKey";
    /**
     * 二维码的ticket，可用来换取二维码图片
     */
    public final static String Ticket="Ticket";
    /**
     * 地理位置纬度
     */
    public final static String Latitude="Latitude";
    /**
     * 地理位置经度
     */
    public final static String Longitude="Longitude";
    /**
     * 地理位置精度
     */
    public final static String Precision="Precision";
    /**
     * 文本消息正文
     */
    public final static String Content="Content";
    /**
     * 
     */
    public final static String CLICK="CLICK";
    
    
    
    public final static String image="image";
    public final static String video="video";
    public final static String voice="voice";
    public final static String thumb="thumb";
    
}
