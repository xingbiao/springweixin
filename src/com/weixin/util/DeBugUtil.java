package com.weixin.util;

import java.lang.reflect.Method;
/**
 * 调试类    输出消息的内容
 * @author i-shaof
 *
 */
public class DeBugUtil {
	public static <T> void log(T t){
		Method[] methods =t.getClass().getMethods();
		System.out.println("=======================");
		for (Method method : methods) {
			if(method.getName().indexOf("get")!=-1){
				try {
					System.out.println(method.getName()+":"+method.invoke(t,null).toString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
				} 
			}
		}
		System.out.println("=======================");
	}
}
